import { Request, Response } from "express"
import { getAllWeb, create, update, deleteOne } from "../controllers/web"

const { getToken } = require('../middleware/getToken')

const express = require('express')
const router : any = express.Router()

router.get('/', getToken, getAllWeb)

router.post('/create', getToken, create)
router.patch('/update/:id', getToken, update)
router.delete('/delete/:id', getToken, deleteOne)

module.exports = router
