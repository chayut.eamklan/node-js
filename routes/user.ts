import { Request, Response } from "express"
import { login, createUser } from "../controllers/user"

const express = require('express')
const router : any = express.Router()


router.post('/login', login)
router.post('/createuser', createUser)

module.exports = router