import mongoose from "mongoose";

const userModel = new mongoose.Schema({
    username: String,
    password: String,
    name: String,
    lastname: String,
    imagePath: String
}, {timestamps: true})

module.exports = mongoose.model('user', userModel)