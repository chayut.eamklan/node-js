import mongoose from "mongoose";

const webModel = new mongoose.Schema({
    seq: Number,
    name: String,
    url: String
}, {timestamps: true})

module.exports = mongoose.model('web', webModel)