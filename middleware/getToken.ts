import {Response, Request, NextFunction} from 'express'

var jwt = require('jsonwebtoken');
export const getToken = async (req :Request, res: Response, next: NextFunction) => {
    try {
        if (!req.headers.authorization) {
            res.status(401).json({
                message: "un authorization"
            })
        }
        const token = (req.headers.authorization)?.split(" ")[1]
        if (!jwt.verify(token, 'test-node-js')) {
            res.status(401).json({
                message: "un authorization"
            })
        }
        next();
    } catch (error) {
        console.log(error)
        res.status(500).send("pung")
    }
    
}