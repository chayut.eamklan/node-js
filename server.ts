// src/app.ts
import express, { Express } from 'express'
import { readdirSync } from 'fs';
import 'dotenv/config'
import { log } from 'console';
import { connectMongoDB } from './config/mongodb';

const morgan = require('morgan')
const cors = require('cors')

const app: Express = express()
connectMongoDB()
const port: number = parseInt(process.env.PORT || '3000', 10);

// middleware
app.use(morgan('dev'))
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

readdirSync('./routes').map((r) => {
  app.use('/api', require('./routes/' + r))
})

app.listen(port, () => console.log(`Application is running on port ${port}`))

