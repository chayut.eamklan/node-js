import { Request, Response } from "express"
const webModel = require('../models/web')

export const getAllWeb = async (req :Request, res: Response) => {
    try {
        const webs = await webModel.find({}).exec();
        res.status(200).send(webs)
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}

export const create = async (req :Request, res: Response) => {
    try {
        console.log(req.body)
        const web = await webModel(req.body).save()
        res.status(200).send(web)
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}

export const example = async (req :Request, res: Response) => {
    try {
        res.status(200).json({
           message : "test"
        })
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}

export const update = async (req :Request, res: Response) => {
    try {
        const name = req.params.id
        const updated = await webModel.findOneAndUpdate({name : name}, req.body).exec();
        res.status(200).send(updated)
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}

export const deleteOne = async (req :Request, res: Response) => {
    try {
        const name = req.params.id
        const deleted = await webModel.findOneAndDelete({name: name}).exec();
        res.status(200).send(deleted)
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}


// create
// update
// delete
