import { Request, Response } from "express"
const userModel = require('../models/user')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

export const login = async (req :Request, res: Response) => {
    try {
        console.log(req.body)
        const { username, password} = req.body
        const user = await userModel.findOne({ username }).exec();
        console.log("user", user)
        if (!user) {
            return res.status(400).json({
                message: "username not exist!!!"
            })
        }

        // check password
        const checkPass = bcrypt.compareSync(password, user.password)
        console.log("checkPass", checkPass)
        if (!checkPass) {
            res.status(400).json({
                message: "username or password invalid!"
            })
        }

        const access_token = jwt.sign({user: user}, 'test-node-js', { expiresIn: '1h' }, (err: any, access_token: any)=> {
            if (err) throw err
            res.status(200).json({access_token, user})
        });

    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}

export const createUser = async (req :Request, res: Response) => {
    try {
        const { username, password} = req.body
        const user = await userModel.findOne({ username }).exec();
        if (user) {
            return res.status(400).json({
                message: "username Already exist!!!"
            })
        }

        const salt = bcrypt.genSaltSync(10);

        const created = new userModel({
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, salt),
            name: req.body.name,
            lastname: req.body.last_name,
            imagePath: req.body.image_path
        })


        created.save()


        res.status(200).send(created)
    } catch(err) {
        console.log(err)
        res.status(500).json({
            message : "Server Internal Error"
        })
    }
}

